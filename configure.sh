#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cmake $DIR -DCMAKE_TOOLCHAIN_FILE=$DIR/arm-none-eabi.cmake
